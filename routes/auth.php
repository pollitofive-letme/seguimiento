<?php

Route::get('datos-usuario',['as' => 'datos-usuario','uses' => 'UserController@edit'])->middleware('data-had-been-completed');
Route::post('datos-usuario',['as' => 'datos-usuario','uses' => 'UserController@update'])->middleware('data-had-been-completed');

Route::get('/bienvenido', 'HomeController@index')->name('home');


Route::group(['middleware' => ['data-completed']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/profile',['as' => 'profile','uses' => 'UserController@profile']);
    Route::post('/profile',['as' => 'profile','uses' => 'UserController@updateProfile']);

    Route::group(['middleware' => 'administrador'],function(){
        Route::post('/universidad/activate',['as' => 'universidad.activate','uses' => 'UniversidadesController@activate']);
        Route::resource('/universidad', 'UniversidadesController');

        Route::post('/carrera/activate',['as' => 'carrera.activate','uses' => 'CarrerasController@activate']);
        Route::resource('/carrera','CarrerasController');

        Route::post('/materia/activate',['as' => 'materia.activate','uses' => 'MateriasController@activate']);
        Route::resource('/materia','MateriasController')->parameters(['materia' => 'materia']);;
    });
});
