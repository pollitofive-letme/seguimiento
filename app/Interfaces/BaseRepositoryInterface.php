<?php

namespace App\Interfaces;

interface BaseRepositoryInterface
{
    function getModel();
}
