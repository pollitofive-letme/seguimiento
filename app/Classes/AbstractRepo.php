<?php

namespace App\Classes;

abstract class AbstractRepo
{
    abstract function getModel();

    public function find($id)
    {
        return $this->getModel()->whereId($id);
    }

    public function delete($id)
    {
        $this->getModel()->whereId($id)->delete();
    }

    public function all()
    {
        return $this->getModel()->withTrashed()->get();
    }

    public function activate($id)
    {
        $model = $this->getModel()->whereId($id)->withTrashed()->first();
        $model->restore();
    }

}
