<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrera extends Model
{
    protected $table = 'carreras';
    protected $guarded = [];
    use SoftDeletes;

    protected function universidad()
    {
        return $this->hasOneThrough(Universidad::class,
            CarrerasXUniversidad::class,
            'universidad_id',
            'id'
        );
    }
}
