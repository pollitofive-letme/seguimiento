<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarrerasXUniversidad extends Model
{
    protected $table = 'carrerasxuniversidad';
    protected $fillable = ['carrera_id','universidad_id'];
}
