<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Universidad extends Model
{
    protected $table = 'universidades';
    protected $fillable = ['nombre','descripcion_corta','created_at'];

    use SoftDeletes;

    protected function carreras()
    {
        return $this->hasManyThrough(Carrera::class,
            CarrerasXUniversidad::class,
            'carrera_id',
            'id'
            );
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('d/m/Y h:i:s A');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::parse($this->attributes['updated_at'])->format('d/m/Y h:i:s A');
    }

}
