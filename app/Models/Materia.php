<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materia extends Model
{
    protected $table = 'materias';
    protected $guarded = [];
    use SoftDeletes;
}
