<?php

namespace App\Models;

use App\Notifications\{ResetPassword,VerifyEmail};
use Illuminate\Contracts\Auth\{MustVerifyEmail,CanResetPassword};
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable implements MustVerifyEmail,CanResetPassword
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'legajo','nombre','apellido','email','dni','password','tipo','estado','genero'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail); // my notification
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function mustCompleteData() : bool
    {
        if ($this->legajo > 0 && $this->nombre != '' && $this->apellido != '' && $this->dni != '' && $this->email != '') {
            return false;
        }

        return true;
    }

    public function getNombreCompletoAttribute()
    {
        return $this->nombre." ".$this->apellido;
    }

    public function esAdministrador()
    {
        return $this->tipo === 1;
    }



}
