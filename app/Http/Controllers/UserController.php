<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserMustCompleteDataRequest;
use App\Http\Requests\UserProfileRequest;
use App\Repositories\RepoUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    private RepoUser $repoUser;

    public function __construct(RepoUser $repoUser)
    {
        $this->repoUser = $repoUser;
    }

    public function edit()
    {
        return view('users.form-must-complete-data');
    }

    public function update(UserMustCompleteDataRequest $request)
    {
        $this->repoUser->update($request);
        session()->flash('success', 'Datos actualizados correctamente');
        return view('home');
    }

    public function profile()
    {
        return view('users.profile');
    }

    public function updateProfile(UserProfileRequest $request)
    {
        $this->repoUser->update($request);
        return Response::json(['success' => true], 200);
    }
}
