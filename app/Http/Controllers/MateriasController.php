<?php

namespace App\Http\Controllers;

use App\Http\Requests\MateriaRequest;
use App\Models\Materia;
use App\Repositories\RepoMateria;
use Illuminate\Database\QueryException as QueryException;
use Illuminate\Http\Request;

class MateriasController extends Controller
{
    private RepoMateria $repoMateria;

    public function __construct(RepoMateria $repoMateria)
    {
        $this->repoMateria = $repoMateria;
    }

    public function index()
    {
        $materias = [];
        return view('materias.index',compact('materias'));
    }

    public function create()
    {
        return view('materias.create');
    }

    public function store(MateriaRequest $request)
    {
        try {
            $this->repoMateria->create($request);
        } catch (QueryException $ex) {
            return $this->serverErrorResponse();
        }

        return $this->successfullResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Materia $materia)
    {
        return view('materias.edit',compact('materia'));
    }

    public function update(MateriaRequest $request, $id)
    {
        try {
            $this->repoMateria->update($request,$id);
        } catch (QueryException $ex) {
            return $this->serverErrorResponse();
        }

        return $this->successfullResponse();
    }

    public function destroy($id)
    {
        $this->repoMateria->delete($id);
        return $this->successfullResponse();
    }

    public function activate(Request $request)
    {
        $this->repoMateria->activate($request->get('id'));
        return $this->successfullResponse();
    }
}
