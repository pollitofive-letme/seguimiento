<?php

namespace App\Http\Controllers;

use App\Http\Requests\UniversidadRequest;
use App\Models\Universidad;
use App\Repositories\RepoUniversidad;
use Illuminate\Http\Request;

class UniversidadesController extends Controller
{
    private RepoUniversidad $repoUniversidad;

    public function __construct(RepoUniversidad $repoUniversidad)
    {
        $this->repoUniversidad = $repoUniversidad;
    }

    public function index()
    {
        return view('universidades.index');
    }

    public function create()
    {
        return view('universidades.create');
    }

    public function store(UniversidadRequest $request)
    {
        $this->repoUniversidad->create($request);
        return $this->successfullResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Universidad $universidad)
    {
        return view('universidades.edit',compact('universidad'));
    }

    public function update(UniversidadRequest $request, $id)
    {
        $this->repoUniversidad->update($request,$id);
        return $this->successfullResponse();
    }

    public function destroy($id)
    {
        $this->repoUniversidad->delete($id);
        return $this->successfullResponse();
    }

    public function activate(Request $request)
    {
        $this->repoUniversidad->activate($request->get('id'));
        return $this->successfullResponse();
    }
}
