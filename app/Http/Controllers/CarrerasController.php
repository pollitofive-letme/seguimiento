<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarreraRequest;
use App\Models\Carrera;
use App\Repositories\RepoCarrera;
use Illuminate\Database\QueryException as QueryException;
use Illuminate\Http\Request;
use Mockery\Exception;

class CarrerasController extends Controller
{
    /**
     * @var RepoCarrera
     */
    private RepoCarrera $repoCarrera;

    public function __construct(RepoCarrera $repoCarrera)
    {
        $this->repoCarrera = $repoCarrera;
    }

    public function index()
    {
        return view("carreras.index");
    }

    public function create()
    {
        return view("carreras.create");
    }

    public function store(CarreraRequest $request)
    {
        try {
            $this->repoCarrera->create($request);
        } catch (QueryException $ex) {
            return $this->serverErrorResponse();
        }

        return $this->successfullResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Carrera $carrera)
    {
        return view('carreras.edit',compact('carrera'));
    }

    public function update(CarreraRequest $request, $id)
    {
        try {
            $this->repoCarrera->update($request,$id);
        } catch (QueryException $ex) {
            return $this->serverErrorResponse();
        }

        return $this->successfullResponse();
    }

    public function destroy($id)
    {
        $this->repoCarrera->delete($id);
        return $this->successfullResponse();
    }

    public function activate(Request $request)
    {
        $this->repoCarrera->activate($request->get('id'));
        return $this->successfullResponse();
    }
}
