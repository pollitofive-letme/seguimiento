<?php

namespace App\Http\Requests;

use App\Traits\ConvertErrorsToJSON;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserProfileRequest extends FormRequest
{
    use ConvertErrorsToJSON;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'legajo' => ['required','numeric','unique:users,legajo,'.Auth::id()],
            'nombre' => 'required',
            'apellido' => 'required',
            'dni' => ['required','numeric','unique:users,dni,'.Auth::id()],
            'email' => ['required','email','unique:users,email,'.Auth::id()],
            'genero' => 'required'
        ];
    }

}
