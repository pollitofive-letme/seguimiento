<?php

namespace App\Http\Requests;

use App\Traits\ConvertErrorsToJSON;
use Illuminate\Foundation\Http\FormRequest;

class CarreraRequest extends FormRequest
{
    use ConvertErrorsToJSON;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'descripcion_corta' => 'required',
            'modalidad' => 'required',
            'duracion' => ['required','numeric'],
        ];
    }
}
