<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserMustCompleteData
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if ( Auth::user()->mustCompleteData()) {
                return redirect('datos-usuario');
            }
        }

        return $next($request);
    }
}
