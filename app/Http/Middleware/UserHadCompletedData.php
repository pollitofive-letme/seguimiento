<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserHadCompletedData
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (! Auth::user()->mustCompleteData()) {
                return redirect('/');
            }
        }

        return $next($request);
    }
}
