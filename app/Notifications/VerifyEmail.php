<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use Illuminate\Auth\Notifications\VerifyEmail as VerifyEmailBase;

class VerifyEmail extends VerifyEmailBase implements ShouldQueue
{
    use Queueable;

    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }

        return (new MailMessage)
            ->subject('Verificar dirección de correo electrónico')
            ->greeting('Hola')
            ->line('Para terminar de configurar su cuenta y comenzar a el '.config('app.name_system').', confirme que tenemos el correo electrónico correcto para usted.')
            ->action(
                'Verificar su correo electrónico',
                $this->verificationUrl($notifiable)
            )
            ->line('Si no creó una cuenta, no se requiere ninguna otra acción.');
    }
}
