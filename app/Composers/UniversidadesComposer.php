<?php

namespace App\Composers;

use App\Repositories\RepoUniversidad;
use Illuminate\View\View;

class UniversidadesComposer
{
    private RepoUniversidad $repoUniversidad;

    public function __construct(RepoUniversidad $repoUniversidad)
    {
        $this->repoUniversidad = $repoUniversidad;
    }

    public function compose(View $view)
    {
        $view->with('universidades', $this->repoUniversidad->all());
    }
}
