<?php

namespace App\Composers;

use App\Repositories\RepoCarrera;
use Illuminate\View\View;

class CarrerasComposer
{
    private RepoCarrera $repoCarrera;

    public function __construct(RepoCarrera $repoCarrera)
    {
        $this->repoCarrera = $repoCarrera;
    }

    public function compose(View $view)
    {
        $view->with('carreras', $this->repoCarrera->all());
    }
}
