<?php

namespace App\Composers;

use App\Repositories\RepoMateria;
use Illuminate\View\View;

class MateriasComposer
{
    private RepoMateria $repoMateria;

    public function __construct(RepoMateria $repoMateria)
    {
        $this->repoMateria = $repoMateria;
    }

    public function compose(View $view)
    {
        $view->with('materias', $this->repoMateria->all());
    }
}
