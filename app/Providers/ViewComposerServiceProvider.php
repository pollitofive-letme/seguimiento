<?php

namespace App\Providers;

use App\Composers\CarrerasComposer;
use App\Composers\MateriasComposer;
use App\Composers\UniversidadesComposer;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \View::composer('universidades/index',UniversidadesComposer::class);
        \View::composer('carreras/index',CarrerasComposer::class);
        \View::composer('materias/index',MateriasComposer::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
