<?php

namespace App\Repositories;

use App\Classes\AbstractRepo;
use App\Interfaces\BaseRepositoryInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RepoUser extends AbstractRepo implements BaseRepositoryInterface
{
    function getModel()
    {
        return new User();
    }

    public function update(Request $request)
    {
        $this->getModel()->where('id',Auth::id())
            ->update([
                'legajo' => $request->get('legajo') ?? Auth::user()->legajo,
                'nombre' => $request->get('nombre'),
                'apellido' => $request->get('apellido'),
                'dni' => $request->get('dni'),
                'email' => $request->get('email') ?? Auth::user()->email,
                'genero' => $request->get('genero') ?? Auth::user()->genero
            ]);
    }
}
