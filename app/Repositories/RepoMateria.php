<?php


namespace App\Repositories;


use App\Classes\AbstractRepo;
use App\Interfaces\BaseRepositoryInterface;
use App\Models\Materia;

class RepoMateria extends AbstractRepo implements BaseRepositoryInterface
{

    function getModel()
    {
        return new Materia();
    }

    public function create($request)
    {
        Materia::create($this->getArray($request));
    }


    public function update($request,$id)
    {
        $this->getModel()->where('id',$id)->update($this->getArray($request));
    }

    /**
     * @param $request
     * @return array
     */
    private function getArray($request): array
    {
        return [
            'nombre' => $request->get('nombre'),
            'descripcion_corta' => $request->get('descripcion_corta'),
            'horas_semanales' => $request->get('horas_semanales'),
            'horas_cuatrimestrales' => $request->get('horas_cuatrimestrales'),
            'anio' => $request->get('anio'),
            'cuatrimestre' => $request->get('cuatrimestre'),
        ];
    }
}
