<?php


namespace App\Repositories;


use App\Classes\AbstractRepo;
use App\Interfaces\BaseRepositoryInterface;
use App\Models\Universidad;

class RepoUniversidad extends AbstractRepo implements BaseRepositoryInterface
{

    function getModel()
    {
        return new Universidad();
    }

    public function create($request)
    {
        Universidad::create([
            'nombre' => $request->get('nombre'),
            'descripcion_corta' => $request->get('descripcion_corta'),
        ]);
    }

    public function update($request,$id)
    {
        $this->getModel()->where('id',$id)->update([
            'nombre' => $request->get('nombre'),
            'descripcion_corta' => $request->get('descripcion_corta')
        ]);
    }

}
