<?php


namespace App\Repositories;


use App\Classes\AbstractRepo;
use App\Interfaces\BaseRepositoryInterface;
use App\Models\Carrera;

class RepoCarrera extends AbstractRepo implements BaseRepositoryInterface
{

    function getModel()
    {
        return new Carrera();
    }

    public function create($request)
    {
        Carrera::create([
            'nombre' => $request->get('nombre'),
            'descripcion_corta' => $request->get('descripcion_corta'),
            'duracion' => $request->get('duracion'),
            'modalidad' => $request->get('modalidad'),
        ]);
    }


    public function update($request,$id)
    {
        $this->getModel()->where('id',$id)->update([
            'nombre' => $request->get('nombre'),
            'descripcion_corta' => $request->get('descripcion_corta'),
            'duracion' => $request->get('duracion'),
            'modalidad' => $request->get('modalidad'),
        ]);
    }

}
