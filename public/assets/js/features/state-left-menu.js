$(".menu-hamburger").on("click", function(e) {
    e.preventDefault();
    $("body").toggleClass("toggle-menu");
    $(".menu-hamburger img").toggle();
});
/* -- Menu Topbar Hamburger -- */
$(".topbar-toggle-hamburger").on("click", function(e) {
    e.preventDefault();
    $("body").toggleClass("topbar-toggle-menu");
    $(".topbar-toggle-hamburger img").toggle();
});
/* -- Media Size -- */
function mediaSize() {
    if (window.matchMedia('(max-width: 767px)').matches) {
        $("body").removeClass("toggle-menu");
        $(".menu-hamburger img.menu-hamburger-close").hide();
        $(".menu-hamburger img.menu-hamburger-collapse").show();
    }
};
mediaSize();

let menu_state = localStorage.getItem('menu-state');
if (menu_state) {

    if(menu_state === 'opened') {
        document.getElementById('menu-opened').style.display = 'none';
        document.getElementById('menu-closed').style.display = 'inline';
        document.querySelector('body').classList.remove('toggle-menu');
    } else {
        document.getElementById('menu-opened').style.display = 'inline';
        document.getElementById('menu-closed').style.display = 'none';
        document.querySelector('body').classList.add('toggle-menu');
    }
}

document.getElementById('toggle-menu').addEventListener('click',(event) =>{
    if (localStorage.getItem('menu-state') == 'opened') {
        localStorage.setItem('menu-state', 'closed');
        return;
    }

    localStorage.setItem('menu-state', 'opened');
});
