function save(route,data,redirect)
{
    async function anotherFunction(redirect) {
            const res = await fetch(route,{
                method : 'post',
                body: data
            });

            var resu = await res.json();

            if(res.status === 500) {
                new PNotify({
                    title: 'Atención', text: 'Error en el servidor, por favor, contacte al administrador', type: 'primary'
                });
                return;
            }


            if(res.status !== 200) {
                let message = "";

                for (var [key, value] of Object.entries(resu.errors)) {
                    document.getElementsByName(key)[0].focus()
                    message += value + "\n";
                }

                new PNotify({
                    title: 'Errores encontrados', text: message, type: 'warning'
                });
                document.getElementById("btnSave").disabled = false;
                return;
            }

            new PNotify({
                title: 'Atención', text: 'Datos guardados correctamente', type: 'primary'
            });

            if(redirect) {
                setTimeout(function(){
                    location.href = redirect
                },1000)
            }

    }

    anotherFunction(redirect)
}
