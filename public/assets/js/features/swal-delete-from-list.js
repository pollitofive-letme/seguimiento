function swalDeleteFromList(description,url,destination){
    swal({
        title: '¿Estas seguro?',
        text: `Estas a punto de eliminar:\n ${description}`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger m-l-10',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then(function () {
        fetch(url,{
            method: 'delete',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-Token": document.getElementsByName('_token')[0].value
            },
        }).then(function(){
            swal(
                '¡Eliminado!',
                'El registro fue eliminado correctamente.',
                'success'
            );
            setTimeout(function() {
                    location.href = destination
                }, 1000
            )

        });
    });
}

