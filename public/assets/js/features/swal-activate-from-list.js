function swalActivateFromList(id,description,url,destination){
    swal({
        title: '¿Estas seguro?',
        text: `Estas a punto de activar:\n ${description}`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger m-l-10',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then(function () {
        fetch(url,{
            method: 'post',
            body: JSON.stringify({id: id}),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-Token": document.getElementsByName('_token')[0].value
            },
        }).then(function(){
            swal(
                '¡Activado!',
                'El registro fue activado correctamente.',
                'success'
            );
            setTimeout(() => location.href = destination, 1000)

        });
    });
}

