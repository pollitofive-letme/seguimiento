<?php


namespace Tests\Traits;
use App\Models\User;
use Faker;
use Illuminate\Support\Facades\Hash;

trait UserTrait
{

    public function generateDataUser(array $data=[])
    {
        $faker = Faker\Factory::create();

        return [
            'legajo'   => ($data['legajo']) ?? $faker->numberBetween(1,20000),
            'nombre'   => ($data['nombre']) ?? $faker->name,
            'apellido' => ($data['apellido']) ?? $faker->lastName,
            'email'    => ($data['email']) ?? $faker->email,
            'dni'      => ($data['dni']) ?? $faker->numberBetween(8000000,48000000),
            'password' => (isset($data['password'])) ? bcrypt($data['password']) : bcrypt('123456'),
            'tipo'     => ($data['tipo']) ?? 2,
            'estado'   => ($data['estado']) ?? 0
        ];
    }

    public function generateDataUserForRegister(array $data=[])
    {
        $faker = Faker\Factory::create();
        $password = ($data['password']) ?? '12345678';
        return [
            'legajo'   => ($data['legajo']) ?? $faker->numberBetween(1,20000),
            'email'    => ($data['email']) ?? $faker->email,
            'password' => $password,
            'password_confirmation' => $password,
        ];
    }

    public function generateUserAdministrator()
    {
        return factory(User::class)->create(['tipo' => 1]);
    }

    public function generateUserNormal()
    {
        return factory(User::class)->create(['tipo' => 2]);
    }
}
