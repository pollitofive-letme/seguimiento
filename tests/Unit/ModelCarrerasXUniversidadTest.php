<?php

namespace Tests\Unit;

use App\Models\Carrera;
use App\Models\CarrerasXUniversidad;
use App\Models\Universidad;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ModelCarrerasXUniversidadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function check_careerxuniversity()
    {
        $university = factory(Universidad::class)->create();
        $career = factory(Carrera::class)->create();

        CarrerasXUniversidad::create([
            'carrera_id' => $career->id,
            'universidad_id' => $university->id
        ]);

        $this->assertDatabaseHas('carrerasxuniversidad',[
            'carrera_id' => $career->id,
            'universidad_id' => $university->id
        ]);
    }

}
