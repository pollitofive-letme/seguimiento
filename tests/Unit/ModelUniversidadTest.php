<?php

namespace Tests\Unit;

use App\Models\Carrera;
use App\Models\CarrerasXUniversidad;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Universidad;

class ModelUniversidadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function check_university()
    {
        $university = factory(Universidad::class)->create();

        $this->assertDatabaseHas('universidades',[
            'id' => $university->id,
            'nombre' => $university->nombre,
            'descripcion_corta' => $university->descripcion_corta,
        ]);
    }

    /** @test */
    function an_university_has_careers()
    {
        $university = factory(Universidad::class)->create();
        $career = factory(Carrera::class)->create();

        CarrerasXUniversidad::create([
            'universidad_id' => $university->id,
            'carrera_id' => $career->id
        ]);
        $this->assertEquals(1, $university->carreras->count());
    }


}
