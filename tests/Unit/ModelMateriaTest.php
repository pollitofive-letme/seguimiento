<?php

namespace Tests\Feature;

use App\Models\Materia;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ModelMateriaTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    function check_subject_fields()
    {
        $subject = factory(Materia::class)->create();

        $this->assertDatabaseHas('materias',[
            'id'                      => $subject->id,
            'nombre'                  => $subject->nombre,
            'descripcion_corta'       => $subject->descripcion_corta,
            'horas_semanales'         => $subject->horas_semanales,
            'cuatrimestre'            => $subject->cuatrimestre,
            'anio'                    => $subject->anio,
            'horas_cuatrimestrales'   => $subject->horas_cuatrimestrales,
        ]);
    }


}
