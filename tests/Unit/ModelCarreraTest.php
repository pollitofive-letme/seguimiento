<?php

namespace Tests\Unit;

use App\Models\CarrerasXUniversidad;
use App\Models\Universidad;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Carrera;

class ModelCarreraTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function check_career()
    {
        $career = factory(Carrera::class)->create();

        $this->assertDatabaseHas('carreras',[
            'id'                => $career->id,
            'nombre'            => $career->nombre,
            'descripcion_corta' => $career->descripcion_corta,
            'duracion'          => $career->duracion,
            'modalidad'         => $career->modalidad,
        ]);
    }

    /** @test */
    function a_career_belongs_to_an_university()
    {
        $university = factory(Universidad::class)->create();
        $career = factory(Carrera::class)->create();

        CarrerasXUniversidad::create([
            'universidad_id' => $university->id,
            'carrera_id' => $career->id
        ]);

        $this->assertEquals(1, $career->universidad->count());
    }



}
