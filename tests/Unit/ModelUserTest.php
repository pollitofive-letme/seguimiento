<?php

namespace Tests\Unit;

use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tests\Traits\UserTrait;
use Illuminate\Support\Facades\Notification;

class ModelUserTest extends TestCase
{
    use DatabaseMigrations,UserTrait;

    /** @test */
    function check_fields_user_table_test()
    {
        $this->withoutExceptionHandling();
        $data = [
            'legajo' => 14461,
            'nombre' => 'Damian',
            'apellido' => 'Ladiani',
            'email' => 'damianladiani@gmail.com',
            'dni' => '33794702',
            'password' => Hash::make('123456'),
            'tipo' => 1,
            'estado' => 0];

        User::create($data);
        $data['id'] = 1;

        $this->assertDatabaseHas('users', $data);

        $data = [
            'legajo' => 14462,
            'nombre' => 'Damian',
            'apellido' => 'Ladiani',
            'email' => 'damianladiani@gmail.com2',
            'dni' => '33794702',
            'password' => Hash::make('123456'),
            'tipo' => 1,
            'estado' => 0
        ];

        User::create($data);
        $data['id'] = 2;

        $this->assertDatabaseHas('users', $data);

    }

    /** @test */
    function an_user_must_verify_email()
    {
        $data = $this->generateDataUser();
        $user = User::create($data);
        $this->assertTrue($user instanceof MustVerifyEmail);
    }

    /** @test */
    function test_send_email_verification_notification()
    {
        Notification::fake();
        $data = $this->generateDataUser();
        $user = User::create($data);
        $user->sendEmailVerificationNotification();
        Notification::assertTimesSent(1,VerifyEmail::class);
    }

    /** @test */
    function test_send_password_reset_notification()
    {
        Notification::fake();
        $data = $this->generateDataUser();
        $user = User::create($data);
        $user->sendPasswordResetNotification($token = bcrypt(123456));
        Notification::assertTimesSent(1,ResetPassword::class);
    }

    /** @test */
    function method_mustCompleteData()
    {
        $data = $this->generateDataUserForRegister();
        $user = User::create($data);
        $this->assertTrue($user->mustCompleteData());

        $user = factory(User::class)->create();
        $this->assertFalse($user->mustCompleteData());

    }

    /** @test */
    function method_esAdministrador()
    {
        $user = factory(User::class)->create([
            'tipo' => 1
        ]);

        $this->assertTrue($user->esAdministrador());

        $user = factory(User::class)->create([
            'tipo' => 2
        ]);

        $this->assertFalse($user->mustCompleteData());

    }

}
