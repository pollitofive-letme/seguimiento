<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InicioTest extends TestCase
{
    /** @test */
    public function a_guest_is_redirected_to_the_login_when_he_goes_to_the_index()
    {
        $response = $this->get('/');
        $response->assertRedirect('login');
    }

    /** @test */
    function a_guest_sees_the_login()
    {
        $this->get('/login')
            ->assertSee(config('app.name_system'))
            ->assertSee('Ingresa tu legajo')
            ->assertSee('Ingresa tu contraseña')
            ->assertSee('¿Olvidaste tu contraseña?')
            ->assertSee('Registrarse')
            ->assertSee('Ingresar')
            ->assertDontSee('Laravel')
            ->assertDontSee('Login')
            ->assertDontSee('Register')
            ->assertDontSee('Remember Me')
            ->assertSee('login');
    }

}
