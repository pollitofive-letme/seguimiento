<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Faker;
class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function an_user_can_see_the_page()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->get('profile')
            ->assertSee('Legajo')
            ->assertSee('Nombre')
            ->assertSee('Apellido')
            ->assertSee('E-mail')
            ->assertSee('Género')
        ;
    }

    /** @test */
    function an_user_sees_his_data_in_the_form()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->get('profile')
            ->assertSee($user->legajo)
            ->assertSee($user->nombre)
            ->assertSee($user->apellido)
            ->assertSee($user->dni)
            ->assertSee($user->email);
    }

    /** @test */
    function an_user_can_update_his_data()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $data = $this->getDataForm([
            'nombre' => 'Damián',
            'apellido' => 'Ladiani',
            'dni' => '33794702',
            'genero' => 'masculino'
        ]);
        $this->actingAs($user)->post('profile',$data)->assertStatus(200);

        $this->assertDatabaseHas('users',$data);
    }

    /** @test */
    function the_update_profile_requires_a_legajo()
    {
        $field = 'legajo';
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $data = $this->getDataForm([$field => '']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','apellido','dni','email'])
            ->assertJsonValidationErrors([$field])
        ;

        $this->assertDatabaseHas('users',[
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'dni' => $user->dni,
        ]);
    }

    /** @test */
    function an_user_cant_take_same_legajo_than_other()
    {
        $this->withoutExceptionHandling();
        $field = 'legajo';
        factory(User::class)->create([$field => '14461']);
        $user = factory(User::class)->create([$field => '14462']);

        $data = $this->getDataForm([$field => '14461']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','apellido','dni','email'])
            ->assertJsonValidationErrors([$field]);

        $this->assertDatabaseHas('users',[
            'legajo' => $user->legajo,
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'dni' => $user->dni,
            'email' => $user->email,
        ]);
    }

    /** @test */
    function the_update_profile_accept_the_same_legajo()
    {
        $field = 'legajo';
        $user = factory(User::class)->create([$field => '14461']);
        $data = $this->getDataForm([$field => '14461']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(200);

        $this->assertDatabaseHas('users',$data);
    }

    /** @test */
    function the_update_profile_requires_a_name()
    {
        $field = 'nombre';
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $data = $this->getDataForm([$field => '']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['apellido','dni'])
            ->assertJsonValidationErrors([$field])
        ;

        $this->assertDatabaseHas('users',[
           'nombre' => $user->nombre,
           'apellido' => $user->apellido,
           'dni' => $user->dni,
        ]);
    }

    /** @test */
    function the_update_profile_requires_a_lastname()
    {
        $field = 'apellido';
        $user = factory(User::class)->create();
        $data = $this->getDataForm([$field => '']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','dni'])
            ->assertJsonValidationErrors([$field])
        ;

        $this->assertDatabaseHas('users',[
           'nombre' => $user->nombre,
           'apellido' => $user->apellido,
           'dni' => $user->dni,
        ]);
    }
    /** @test */
    function the_update_profile_requires_a_dni()
    {
        $field = 'dni';
        $user = factory(User::class)->create();
        $data = $this->getDataForm([$field => '']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','apellido'])
            ->assertJsonValidationErrors([$field])
        ;

        $this->assertDatabaseHas('users',[
           'nombre' => $user->nombre,
           'apellido' => $user->apellido,
           'dni' => $user->dni,
        ]);
    }

    /** @test */
    function the_update_profile_accept_the_same_dni()
    {
        $field = 'dni';
        $user = factory(User::class)->create([$field => '33794702']);
        $data = $this->getDataForm([$field => '33794702']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(200);

        $this->assertDatabaseHas('users',$data);
    }

    /** @test */
    function the_update_profile_must_have_numeric_dni()
    {
        $field = 'dni';
        $user = factory(User::class)->create();
        $data = $this->getDataForm([$field => 'sdsadada']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','apellido','email'])
            ->assertJsonValidationErrors([$field])
        ;

        $this->assertDatabaseHas('users',[
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'email' => $user->email,
        ]);
    }

    /** @test */
    function an_user_cant_take_same_dni_than_other()
    {
        $field = 'dni';
        factory(User::class)->create([$field => '33794702']);
        $user = factory(User::class)->create([$field => '33794701']);

        $data = $this->getDataForm([$field => '33794702']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','apellido','legajo','email'])
            ->assertJsonValidationErrors([$field]);

        $this->assertDatabaseHas('users',[
            'legajo' => $user->legajo,
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'dni' => $user->dni,
            'email' => $user->email,
        ]);
    }


    /** @test */
    function the_update_profile_requires_an_email()
    {
        $field = 'email';
        $user = factory(User::class)->create();
        $data = $this->getDataForm([$field => '']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','apellido','dni'])
            ->assertJsonValidationErrors([$field])
        ;

        $this->assertDatabaseHas('users',[
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'dni' => $user->dni,
            'email' => $user->email,
        ]);
    }

    /** @test */
    function the_update_profile_accept_the_same_email()
    {
        $field = 'email';
        $user = factory(User::class)->create(['email' => 'damianladiani@gmail.com']);
        $data = $this->getDataForm([$field => 'damianladiani@gmail.com']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(200);

        $this->assertDatabaseHas('users',$data);
    }

    /** @test */
    function an_user_cant_take_same_email_than_other()
    {
        $field = 'email';
        factory(User::class)->create([$field => 'damianladiani@gmail.com']);
        $user = factory(User::class)->create([$field => 'damianladiani1@gmail.com']);

        $data = $this->getDataForm([$field => 'damianladiani@gmail.com']);

        $this->actingAs($user)->post('profile',$data)
            ->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','apellido','legajo','legajo'])
            ->assertJsonValidationErrors([$field]);

        $this->assertDatabaseHas('users',[
            'legajo' => $user->legajo,
            'nombre' => $user->nombre,
            'apellido' => $user->apellido,
            'dni' => $user->dni,
            'email' => $user->email,
        ]);
    }


    private function getDataForm($data=array())
    {
        $faker = Faker\Factory::create('es_ar');
        return [
            'legajo' => $data['legajo'] ?? $faker->numberBetween(10000,99999),
            'nombre' => $data['nombre'] ?? $faker->name,
            'apellido' => $data['apellido'] ?? $faker->lastname,
            'dni' => $data['dni'] ?? $faker->numberBetween(8000000,48000000),
            'email' => $data['email'] ?? $faker->email,
            'genero' => $data['genmero'] ?? $faker->randomElement(['masculino','femenino'])
        ];
    }

    /** @test */
    function an_user_sees_the_correct_gender()
    {
        $user = factory(User::class)->create([
            'genero' => 'masculino'
        ]);

        $this->followingRedirects()->actingAs($user)->get('/')->assertSee('boy.svg');

        $user = factory(User::class)->create([
            'genero' => 'femenino'
        ]);

        $this->followingRedirects()->actingAs($user)->get('/')->assertSee('girl.svg');
    }


}
