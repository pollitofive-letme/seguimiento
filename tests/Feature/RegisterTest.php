<?php

namespace Tests\Feature;

use App\Notifications\VerifyEmail;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Notifications\Messages\MailMessage;
use Tests\TestCase;
use Tests\Traits\UserTrait;
use Illuminate\Support\Facades\{Event, Mail, Notification};

class RegisterTest extends TestCase
{
    use UserTrait,RefreshDatabase;

    /** @test */
    function a_guest_sees_the_page_register()
    {
        $this->get('/registro')
            ->assertSee(config('app.name_system'))
            ->assertSee('Registrarse en el sistema')
            ->assertSee('Ingresa tu legajo')
            ->assertSee('Ingresa tu email')
            ->assertSee('Ingresa una contraseña')
            ->assertSee('Re ingresa la contraseña')
            ->assertSee('Registrarse!');
    }

    /** @test */
    function a_guest_can_create_an_user()
    {
        $data = $this->generateDataUserForRegister();
        $this->post('registro',$data)->assertStatus(302)->assertRedirect('/bienvenido');

        unset($data['password']);
        unset($data['password_confirmation']);
        $data['tipo'] = 2;
        $data['estado'] = 0;

        $this->assertDatabaseHas('users',$data);

    }

    /** @test */
    function a_register_shot_event()
    {
        Event::fake();
        $data = $this->generateDataUserForRegister();
        $this->post('registro',$data);

        Event::assertDispatched(Registered::class);
    }

    /** @test */
    function a_guest_see_the_welcome_page_after_register()
    {
        $this->followingRedirects()
            ->post('registro',$this->generateDataUserForRegister())
            ->assertSee(config('app.name_short'))
            ->assertSee("Bienvenido al ".config('app.name_system'))
            ->assertSee('Te hemos enviado un mail para confirmar la cuenta.');
    }

    /** @test */
    function an_user_cant_take_an_email_taked()
    {
        $this->withoutExceptionHandling();
        $field = 'email';
        $data = [$field => 'damianladiani@gmail.com'];
        User::create($this->generateDataUserForRegister($data));

        $user2 = $this->generateDataUserForRegister($data);
        $this->post('registro',$user2)->assertSessionHasErrors($field);
    }

    /** @test */
    function an_user_cant_take_an_legajo_taked()
    {
        $field = 'legajo';
        $data = [$field => 14461];
        User::create($this->generateDataUserForRegister($data));

        $user2 = $this->generateDataUserForRegister($data);
        $this->post('registro',$user2)->assertSessionHasErrors($field);
    }
}
