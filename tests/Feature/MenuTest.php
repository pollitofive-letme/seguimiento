<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MenuTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function an_administrator_user_sees_the_administrator_option_in_the_menu()
    {
        $user = factory(User::class)->create([
           'tipo' => 1
        ]);

        $this->followingRedirects()->actingAs($user)->get('/')
            ->assertSee('#v-administracions')
            ->assertSee('id-Universidades-t')
            ->assertSee('id-Carreras-t')
            ->assertSee('id-Materias-t')
            ->assertSee(url('/universidad'))
            ->assertDontSee(url('/universidades'))
            ->assertSee(url('/carrera'))
            ->assertDontSee(url('/carreras'))
        ;
    }

    /** @test */
    function an_user_normal_cant_see_administration_menu()
    {
        $user = factory(User::class)->create([
            'tipo' => 2
        ]);

        $this->followingRedirects()->actingAs($user)->get('/')
            ->assertDontSee('#v-administracions')
            ->assertDontSee('id-Universidades-t')
            ->assertDontSee('id-Carreras-t')
            ->assertDontSee('id-Materias-t')
        ;
    }

}
