<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\UserTrait;

class UserMustCompleteDataTest extends TestCase
{

    use RefreshDatabase,UserTrait;
    /** @test */

    function an_user_must_complete_data_is_redirected()
    {
        $dataUser = $this->generateDataUserForRegister();
        $user = User::create($dataUser);
        $this->actingAs($user)->get("/home")
            ->assertRedirect('datos-usuario');
    }

    /** @test */

    function an_user_most_complete_data_sees_form()
    {
        $dataUser = $this->generateDataUser(['nombre' => '','apellido' => '','dni' => '']);
        $user = User::create($dataUser);
        $this->followingRedirects()->actingAs($user)->get("/datos-usuario")
            ->assertSee('Por favor, completa los siguientes datos antes de continuar')
            ->assertSee('nombre')
            ->assertSee('apellido')
            ->assertSee('dni')
            ->assertSee('genero')
        ;
    }

    /** @test */
    function an_user_can_update_data_after_register()
    {
        $this->withoutExceptionHandling();
        $dataUser = $this->generateDataUser(['nombre' => '','apellido' => '','dni' => '']);
        $user = User::create($dataUser);
        $data = [
            'nombre' => 'Damian',
            'apellido' => 'Ladiani',
            'dni' => '33794702',
            'genero' => 'masculino'
        ];
        $this->actingAs($user)->post('datos-usuario',$data)->assertViewIs("home");

        $this->assertDatabaseHas('users',$data);
    }

    /** @test */
    function an_user_is_redirected_after_update_data()
    {
        $this->withoutExceptionHandling();
        $dataUser = $this->generateDataUser();
        $user = User::create($dataUser);
        $data = [
            'nombre' => 'Damian',
            'apellido' => 'Ladiani',
            'dni' => '33794702',
        ];
        $this->actingAs($user)->post('datos-usuario',$data)->assertRedirect('/');
    }

    /** @test */
    function an_user_with_data_completed_cant_see_page_update_data()
    {
        $dataUser = $this->generateDataUser();
        $user = User::create($dataUser);
        $data = [
            'nombre' => 'Damian',
            'apellido' => 'Ladiani',
            'dni' => '33794702'
        ];
        $this->actingAs($user)->post('datos-usuario',$data);

        $this->get('/datos-usuario')->assertRedirect('/');

    }

}
