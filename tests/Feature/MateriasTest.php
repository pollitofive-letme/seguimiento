<?php

namespace Tests\Feature;

use App\Models\Materia;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\UserTrait;
use Faker;


class MateriasTest extends TestCase
{
    use RefreshDatabase, UserTrait;

    /** @test */
    function an_user_administrator_can_see_the_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->get('materia')
            ->assertSee('Listado de Materias')
            ->assertSee('Nombre')
            ->assertSee('Descripción corta')
            ->assertSee('Horas semanales')
            ->assertSee('Año')
            ->assertSee('Horas cuatrimestrales')
            ->assertSee('Agregar');


    }

    /** @test */
    function an_user_normal_cant_see_the_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserNormal();
        $this->actingAs($user)->get('materia')
            ->assertDontSee('Listado de materias')
            ->assertDontSee('Nombre')
            ->assertDontSee('Descripción corta')
            ->assertDontSee('Horas semanales')
            ->assertDontSee('Año')
            ->assertDontSee('Horas cuatrimestrales')
            ->assertDontSee('Agregar')
            ->assertDontSee('Editar')
            ->assertDontSee('Borrar');

    }

    /** @test */
    function an_administrator_can_see_the_create_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->get('materia/create')
            ->assertSee('Crear materia')
            ->assertSee('Nombre')
            ->assertSee('Descripcion corta')
            ->assertSee('Horas semanales')
            ->assertSee('Horas cuatrimestrales')
            ->assertSee('Cuatrimestre')
            ->assertSee('Año')
            ->assertSee('Guardar');
    }

    /** @test */
    function an_administrator_can_create_a_subject()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post('materia',[
            'nombre' => 'Materia 1',
            'descripcion_corta' => 'car1',
            'horas_semanales' => 4,
            'horas_cuatrimestrales' => 68,
            'cuatrimestre' => 2,
            'anio' => 2
        ])->assertStatus(200)->assertJson(['success' => true]);

        $this->assertDatabaseHas('materias',[
            'nombre' => 'Materia 1',
            'descripcion_corta' => 'car1',
            'horas_semanales' => 4,
            'horas_cuatrimestrales' => 68,
            'cuatrimestre' => 2,
            'anio' => 2
        ]);
    }

    /** @test */
    function the_name_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['nombre' => '']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['descripcion_corta','horas_semanales','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['nombre']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['descripcion_corta','horas_semanales','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['nombre']);
    }

    /** @test */
    function the_short_description_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['descripcion_corta' => '']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['descripcion_corta']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['descripcion_corta']);
    }

    /** @test */
    function the_weekly_hours_are_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['horas_semanales' => '']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','descripcion_corta','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_semanales']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','descripcion_corta','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_semanales']);
    }

    /** @test */
    function the_weekly_hours_must_be_numeric()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['horas_semanales' => 'dfsfds']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','descripcion_corta','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_semanales']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','descripcion_corta','horas_cuatrimestrales','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_semanales']);
    }

    /** @test */
    function the_quarterly_hours_are_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['horas_cuatrimestrales' => '']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_cuatrimestrales']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_cuatrimestrales']);
    }

    /** @test */
    function the_quarterly_hours_must_be_numeric()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['horas_cuatrimestrales' => 'dsadada']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_cuatrimestrales']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','anio','cuatrimestre'])
            ->assertJsonValidationErrors(['horas_cuatrimestrales']);
    }

    /** @test */
    function the_year_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['anio' => '']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','cuatrimestre'])
            ->assertJsonValidationErrors(['anio']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','cuatrimestre'])
            ->assertJsonValidationErrors(['anio']);
    }

    /** @test */
    function the_year_must_be_numeric()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['anio' => 'dsada']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','cuatrimestre'])
            ->assertJsonValidationErrors(['anio']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','cuatrimestre'])
            ->assertJsonValidationErrors(['anio']);
    }

    /** @test */
    function the_quarter_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['cuatrimestre' => '']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','anio'])
            ->assertJsonValidationErrors(['cuatrimestre']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','anio'])
            ->assertJsonValidationErrors(['cuatrimestre']);
    }

    /** @test */
    function the_quarter_must_be_numeric()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataSubject(['cuatrimestre' => 'dsadada']);
        $this->actingAs($user)->post('materia',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','anio'])
            ->assertJsonValidationErrors(['cuatrimestre']);

        $cubject = factory(Materia::class)->create();

        $this->actingAs($user)->put("materia/{$cubject->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','horas_semanales','descripcion_corta','horas_cuatrimestrales','anio'])
            ->assertJsonValidationErrors(['cuatrimestre']);
    }

    /** @test */
    function an_administrator_can_see_the_edit_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $subject = factory(Materia::class)->create();
        $this->actingAs($user)->get("materia/{$subject->id}/edit")
            ->assertSee('Editar materia')
            ->assertSee('Nombre')
            ->assertSee('Descripcion corta')
            ->assertSee('Horas semanales')
            ->assertSee('Horas cuatrimestrales')
            ->assertSee('Año')
            ->assertSee('Cuatrimestre')
            ->assertSee('Guardar')
            ->assertSee($subject->nombre)
            ->assertSee($subject->descripcion_corta)
            ->assertSee($subject->modalidad)
            ->assertSee($subject->duracion);
    }


    /** @test */
    function an_administrator_can_edit_a_subject()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $subject = factory(Materia::class)->create();

        $data = $this->generateDataSubject();

        $this->actingAs($user)->put("materia/{$subject->id}",$data)->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertDatabaseHas('materias',$data);

        $this->assertDatabaseMissing('materias',[
            'nombre' => $subject->nombre,
            'descripcion_corta' => $subject->descripcion_corta,
            'horas_semanales' => $subject->horas_semanales,
            'horas_cuatrimestrales' => $subject->horas_cuatrimestrales,
            'anio'    => $subject->anio,
            'cuatrimestre'    => $subject->cuatrimestre,
        ]);

    }


    /** @test */
    function an_user_administrator_can_delete_a_subject()
    {
        $subject = factory(Materia::class)->create();

        $this->assertDatabaseHas('materias',[
            'id' => $subject->id,
            'nombre' => $subject->nombre,
            'descripcion_corta' => $subject->descripcion_corta,
            'horas_semanales' => $subject->horas_semanales,
            'horas_cuatrimestrales' => $subject->horas_cuatrimestrales,
            'anio'    => $subject->anio,
            'cuatrimestre'    => $subject->cuatrimestre,
        ]);


        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->delete("materia/{$subject->id}")
            ->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertSoftDeleted('materias',[
            'id' => $subject->id,
            'nombre' => $subject->nombre,
            'descripcion_corta' => $subject->descripcion_corta,
            'horas_semanales' => $subject->horas_semanales,
            'horas_cuatrimestrales' => $subject->horas_cuatrimestrales,
            'anio'    => $subject->anio,
            'cuatrimestre'    => $subject->cuatrimestre,
        ]);
    }


    /** @test */
    function an_user_adminstrator_sees_the_list_of_subjects()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();

        $subjects = factory(Materia::class,10)->create();

        $response = $this->actingAs($user)->get('/materia');

        $subjects->each(fn($subject) =>
            $response->assertSee($subject->id)
                    ->assertSee($subject->nombre)
                    ->assertSee($subject->descripcion_corta)
                    ->assertSee($subject->horas_semanales)
                    ->assertSee($subject->horas_cuatrimestrales)
                    ->assertSee($subject->anio)
                    ->assertSee($subject->cuatrimestre)
        );
    }

    /** @test */
    function an_user_administrator_can_activate_a_subject()
    {
        $this->withoutExceptionHandling();
        $subject = factory(Materia::class)->create();

        $subject->delete();

        $this->assertSoftDeleted('materias',[
            'id' => $subject->id,
            'nombre' => $subject->nombre,
            'descripcion_corta' => $subject->descripcion_corta,
            'horas_semanales' => $subject->horas_semanales,
            'horas_cuatrimestrales' => $subject->horas_cuatrimestrales,
            'anio'    => $subject->anio,
            'cuatrimestre'    => $subject->cuatrimestre,
        ]);

        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post("materia/activate",[
            'id' => $subject->id
        ])->assertStatus(200)
          ->assertJson(['success' => true]);

        $this->assertDatabaseHas('materias',[
            'id' => $subject->id,
            'nombre' => $subject->nombre,
            'descripcion_corta' => $subject->descripcion_corta,
            'horas_semanales' => $subject->horas_semanales,
            'horas_cuatrimestrales' => $subject->horas_cuatrimestrales,
            'anio'    => $subject->anio,
            'cuatrimestre'    => $subject->cuatrimestre,
            'deleted_at' => null
        ]);
    }



    public function generateDataSubject(array $data=[])
    {
        $faker = Faker\Factory::create();

        $nombre = $faker->name;
        $horas = [2 => 34,3 => 51,4 => 68,6 => 102,8 => 136];
        $horas_semanales = $faker->randomElement([2,3,4,6,8]);
        $horas_cuatrimestrales = $horas[$horas_semanales];
        return [
            'nombre'   => ($data['nombre']) ?? $nombre,
            'descripcion_corta'   => ($data['descripcion_corta']) ?? substr($nombre,0,3),
            'horas_semanales' => ($data['horas_semanales']) ?? $horas_semanales,
            'horas_cuatrimestrales' => ($data['horas_cuatrimestrales']) ?? $horas_cuatrimestrales,
            'anio'    => ($data['anio']) ?? $faker->randomElement([1,2,3,4,5]),
            'cuatrimestre'    => ($data['cuatrimestre']) ?? $faker->randomElement([1,2]),
        ];
    }


}
