<?php

namespace Tests\Feature;

use App\Models\Carrera;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\UserTrait;
use Faker;

class CarrerasTest extends TestCase
{
    use RefreshDatabase, UserTrait;

    /** @test */
    function an_user_administrator_can_see_the_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->get('carrera')
            ->assertSee('Listado de carreras')
            ->assertSee('Nombre')
            ->assertSee('Descripción corta')
            ->assertSee('Duración')
            ->assertSee('Modalidad')
            ->assertSee('Agregar');

    }

    /** @test */
    function an_user_normal_cant_see_the_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserNormal();
        $this->actingAs($user)->get('carrera')
            ->assertDontSee('Listado de carreras')
            ->assertDontSee('Nombre')
            ->assertDontSee('Descripción corta')
            ->assertDontSee('Duración')
            ->assertDontSee('Modalidad')
            ->assertDontSee('Agregar')
            ->assertDontSee('Editar')
            ->assertDontSee('Borrar');

    }

    /** @test */
    function an_administrator_can_see_the_create_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->get('carrera/create')
            ->assertSee('Crear carrera')
            ->assertSee('Nombre')
            ->assertSee('Descripcion corta')
            ->assertSee('Duracion')
            ->assertSee('Modalidad')
            ->assertSee('Guardar');
    }

    /** @test */
    function an_administrator_can_create_a_career()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post('carrera',[
            'nombre' => 'Carrera 1',
            'descripcion_corta' => 'car1',
            'duracion' => 4,
            'modalidad' => 'Presencial'
        ])->assertStatus(200)->assertJson(['success' => true]);

        $this->assertDatabaseHas('carreras',[
            'nombre' => 'Carrera 1',
            'descripcion_corta' => 'car1',
            'duracion' => 4,
            'modalidad' => 'Presencial'
        ]);
    }

    /** @test */
    function the_name_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataCareer(['nombre' => '']);
        $this->actingAs($user)->post('carrera',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['descripcion_corta','modalidad','duracion'])
            ->assertJsonValidationErrors(['nombre']);

        $career = factory(Carrera::class)->create();

        $this->actingAs($user)->put("carrera/{$career->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['descripcion_corta','modalidad','duracion'])
            ->assertJsonValidationErrors(['nombre']);
    }

    /** @test */
    function the_short_description_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataCareer(['descripcion_corta' => '']);
        $this->actingAs($user)->post('carrera',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','duracion','modalidad'])
            ->assertJsonValidationErrors(['descripcion_corta']);

        $career = factory(Carrera::class)->create();

        $this->actingAs($user)->put("carrera/{$career->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','duracion','modalidad'])
            ->assertJsonValidationErrors(['descripcion_corta']);
    }

    /** @test */
    function the_modality_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataCareer(['modalidad' => '']);
        $this->actingAs($user)->post('carrera',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','duracion','descripcion_corta'])
            ->assertJsonValidationErrors(['modalidad']);

        $career = factory(Carrera::class)->create();

        $this->actingAs($user)->put("carrera/{$career->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','duracion','descripcion_corta'])
            ->assertJsonValidationErrors(['modalidad']);
    }


    /** @test */
    function the_duration_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataCareer(['duracion' => '']);
        $this->actingAs($user)->post('carrera',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','modalidad','descripcion_corta'])
            ->assertJsonValidationErrors(['duracion']);

        $career = factory(Carrera::class)->create();

        $this->actingAs($user)->put("carrera/{$career->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','modalidad','descripcion_corta'])
            ->assertJsonValidationErrors(['duracion']);
    }

    /** @test */
    function the_duracion_must_be_a_number()
    {
        //$this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $data = $this->generateDataCareer(['duracion' => 'dadsad']);
        $this->actingAs($user)->post('carrera',$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','modalidad','descripcion_corta'])
            ->assertJsonValidationErrors(['duracion']);

        $career = factory(Carrera::class)->create();

        $this->actingAs($user)->put("carrera/{$career->id}",$data)->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre','modalidad','descripcion_corta'])
            ->assertJsonValidationErrors(['duracion']);
    }

    /** @test */
    function an_administrator_can_see_the_edit_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $career = factory(Carrera::class)->create();

        $this->actingAs($user)->get("carrera/{$career->id}/edit")
            ->assertSee('Editar carrera')
            ->assertSee('Nombre')
            ->assertSee('Descripcion corta')
            ->assertSee('Modalidad')
            ->assertSee('Duracion')
            ->assertSee('Guardar')
            ->assertSee($career->nombre)
            ->assertSee($career->descripcion_corta)
            ->assertSee($career->modalidad)
            ->assertSee($career->duracion)
        ;
    }

    /** @test */
    function an_administrator_can_edit_a_career()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $career = factory(Carrera::class)->create();

        $data = $this->generateDataCareer();

        $this->actingAs($user)->put("carrera/{$career->id}",$data)->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertDatabaseHas('carreras',$data);

        $this->assertDatabaseMissing('carreras',[
            'nombre' => $career->nombre,
            'descripcion_corta' => $career->descripcion_corta,
            'duracion' => $career->duracion,
            'modalidad' => $career->modalidad,
        ]);

    }

    /** @test */
    function an_user_administrator_can_delete_a_career()
    {
        $career = factory(Carrera::class)->create();

        $this->assertDatabaseHas('carreras',[
            'id' => $career->id,
            'nombre' => $career->nombre,
            'descripcion_corta' => $career->descripcion_corta,
            'duracion' => $career->duracion,
            'modalidad' => $career->modalidad,
        ]);


        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->delete("carrera/{$career->id}")
            ->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertSoftDeleted('carreras',[
            'id' => $career->id,
            'nombre' => $career->nombre,
            'descripcion_corta' => $career->descripcion_corta,
            'duracion' => $career->duracion,
            'modalidad' => $career->modalidad,
        ]);
    }

    /** @test */
    function an_user_adminstrator_sees_the_list_of_careers()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();

        $careers = factory(Carrera::class,10)->create();

        $response = $this->actingAs($user)->get('/carrera');

        $careers->each(fn($career) =>
            $response->assertSee($career->id)
                     ->assertSee($career->nombre)
                     ->assertSee($career->descripcion_corta)
                     ->assertSee($career->duracion)
                     ->assertSee($career->modalidad)
        );
    }

    /** @test */
    function an_user_administrator_can_activate_a_career()
    {
        $this->withoutExceptionHandling();
        $career = factory(Carrera::class)->create();

        $career->delete();

        $this->assertSoftDeleted('carreras',[
            'id' => $career->id,
            'nombre' => $career->nombre,
            'descripcion_corta' => $career->descripcion_corta,
            'duracion' => $career->duracion,
            'modalidad' => $career->modalidad
        ]);

        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post("carrera/activate",[
            'id' => $career->id
        ])
            ->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertDatabaseHas('carreras',[
            'id' => $career->id,
            'nombre' => $career->nombre,
            'descripcion_corta' => $career->descripcion_corta,
            'duracion' => $career->duracion,
            'modalidad' => $career->modalidad,
            'deleted_at' => null
        ]);
    }



    public function generateDataCareer(array $data=[])
    {
        $faker = Faker\Factory::create();

        $nombre = $faker->name;

        return [
            'nombre'   => ($data['nombre']) ?? $nombre,
            'descripcion_corta'   => ($data['descripcion_corta']) ?? substr($nombre,0,3),
            'modalidad' => ($data['modalidad']) ?? $faker->randomElement(['Presencial','A distancia']),
            'duracion'    => ($data['duracion']) ?? $faker->randomElement([1,2,3,4,5]),
        ];
    }



}
