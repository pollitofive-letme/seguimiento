<?php

namespace Tests\Feature;

use App\Models\Universidad;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\UserTrait;

class UniversidadesTest extends TestCase
{
    use RefreshDatabase, UserTrait;

    /** @test */
    function an_user_administrator_can_see_the_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->get('universidad')
            ->assertSee('Listado de universidades')
            ->assertSee('Nombre')
            ->assertSee('Descripción corta')
            ->assertSee('Agregar');

    }

    /** @test */
    function an_user_normal_cant_see_the_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserNormal();
        $this->actingAs($user)->get('universidad')
            ->assertDontSee('Listado de universidades')
            ->assertDontSee('Nombre')
            ->assertDontSee('Descripción corta')
            ->assertDontSee('Agregar')
            ->assertDontSee('Editar')
            ->assertDontSee('Borrar');

    }

    /** @test */
    function an_administrator_can_see_the_create_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->get('universidad/create')
            ->assertSee('Crear universidad')
            ->assertSee('Nombre')
            ->assertSee('Descripción corta')
            ->assertSee('Guardar');
    }

    /** @test */
    function an_administrator_can_create_an_university()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post('universidad',[
            'nombre' => 'Universidad 1',
            'descripcion_corta' => 'uni1',

        ])->assertStatus(200)->assertJson(['success' => true]);

        $this->assertDatabaseHas('universidades',[
            'nombre' => 'Universidad 1',
            'descripcion_corta' => 'uni1',
        ]);
    }

    /** @test */
    function the_name_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post('universidad',[
            'nombre' => '',
            'descripcion_corta' => 'uni1',

        ])->assertStatus(422)
        ->assertJsonMissingValidationErrors(['descripcion_corta'])
        ->assertJsonValidationErrors(['nombre']);

        $university = factory(Universidad::class)->create();

        $this->actingAs($user)->put("universidad/{$university->id}",[
            'nombre' => '',
            'descripcion_corta' => 'uni1',

        ])->assertStatus(422)
            ->assertJsonMissingValidationErrors(['descripcion_corta'])
            ->assertJsonValidationErrors(['nombre']);
    }

    /** @test */
    function the_short_description_is_required()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post('universidad',[
            'nombre' => 'Universidad 1',
            'descripcion_corta' => '',

        ])->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre'])
            ->assertJsonValidationErrors(['descripcion_corta']);

        $university = factory(Universidad::class)->create();

        $this->actingAs($user)->put("universidad/{$university->id}",[
            'nombre' => 'Universidad 1',
            'descripcion_corta' => '',

        ])->assertStatus(422)
            ->assertJsonMissingValidationErrors(['nombre'])
            ->assertJsonValidationErrors(['descripcion_corta']);


    }

    /** @test */
    function an_administrator_can_see_the_edit_page()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $university = factory(Universidad::class)->create();

        $this->actingAs($user)->get("universidad/{$university->id}/edit")
            ->assertSee('Editar universidad')
            ->assertSee('Nombre')
            ->assertSee('Descripción corta')
            ->assertSee('Guardar')
            ->assertSee($university->nombre)
            ->assertSee($university->descripcion_corta)
        ;
    }

    /** @test */
    function an_administrator_can_edit_an_university()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();
        $university = factory(Universidad::class)->create();

        $this->actingAs($user)->put("universidad/{$university->id}",[
            'nombre' => 'Universidad 1',
            'descripcion_corta' => 'uni1',

        ])->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertDatabaseHas('universidades',[
            'nombre' => 'Universidad 1',
            'descripcion_corta' => 'uni1',
        ]);

        $this->assertDatabaseMissing('universidades',[
            'nombre' => $university->nombre,
            'descripcion_corta' => $university->descripcion_corta,
        ]);

    }

    /** @test */
    function an_user_administrator_can_delete_an_university()
    {
        $university = factory(Universidad::class)->create();

        $this->assertDatabaseHas('universidades',[
            'id' => $university->id,
            'nombre' => $university->nombre,
            'descripcion_corta' => $university->descripcion_corta
        ]);


        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->delete("universidad/{$university->id}")
            ->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertSoftDeleted('universidades',[
           'id' => $university->id,
           'nombre' => $university->nombre,
           'descripcion_corta' => $university->descripcion_corta
        ]);
    }

    /** @test */
    function an_user_adminstrator_sees_the_list_of_universities()
    {
        $this->withoutExceptionHandling();
        $user = $this->generateUserAdministrator();

        $universities = factory(Universidad::class,10)->create();

        $response = $this->actingAs($user)->get('/universidad');

        $universities->each(fn($university) =>
            $response->assertSee($university->id)
                ->assertSee($university->nombre)
                ->assertSee($university->descripcion_corta)
        );


    }

    /** @test */
    function an_user_administrator_can_activate_an_university()
    {
        $this->withoutExceptionHandling();
        $university = factory(Universidad::class)->create();

        $university->delete();

        $this->assertSoftDeleted('universidades',[
            'id' => $university->id,
            'nombre' => $university->nombre,
            'descripcion_corta' => $university->descripcion_corta
        ]);

        $user = $this->generateUserAdministrator();
        $this->actingAs($user)->post("universidad/activate",[
            'id' => $university->id
        ])
            ->assertStatus(200)
            ->assertJson(['success' => true]);

        $this->assertDatabaseHas('universidades',[
            'id' => $university->id,
            'nombre' => $university->nombre,
            'descripcion_corta' => $university->descripcion_corta,
            'deleted_at' => null
        ]);
    }

}
