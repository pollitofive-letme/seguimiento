<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\UserTrait;

class LoginTest extends TestCase
{
    use RefreshDatabase,UserTrait;

    /** @test */
    function an_guest_see_page_login()
    {
        $this->get('login')
            ->assertSee(config('app.name_system'))
            ->assertSee('Ingresa tu legajo')
            ->assertSee('Ingresa tu contraseña')
            ->assertSee('Ingresar')
            ->assertSee('Registrarse')
            ->assertSee(' ¿Olvidaste tu contraseña?');
    }

    /** @test */
    function an_user_sees_errors_with_wrong_credentials()
    {
        $this->post('login',[
            'legajo' => 111,
            'password' => 111
        ])->assertSessionHasErrors('legajo');
    }


    /** @test */
    function an_user_can_login()
    {
        User::create($this->generateDataUser([
            'legajo' => '14461',
            'password' => '12345678'
        ]));

        $this->post('login',[
            'legajo' => '14461',
            'password' => '12345678'
        ])->assertRedirect('/home');
    }

}
