<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    /** @test */
    function an_guest_see_page_forgot_password()
    {
        $this->get('password/reset')
            ->assertSee('¿Olvidaste tu contraseña?')
            ->assertSee('Ingresa tu email')
            ->assertSee('Enviar link para recuperar contraseña');
    }
}
