<div class="tab-pane fade show active" id="v-pills-crm" role="tabpanel" aria-labelledby="v-pills-crm-tab">
    <ul class="vertical-menu">
        <li><h5 class="menu-title">Administración</h5></li>
        <x-li-a-left-bar-submenu route="/universidad" imagen="dashboard">Universidades</x-li-a-left-bar-submenu>
        <x-li-a-left-bar-submenu route="/carrera" imagen="reports">Carreras</x-li-a-left-bar-submenu>
        <x-li-a-left-bar-submenu route="/materia" imagen="charts">Materias</x-li-a-left-bar-submenu>
    </ul>
</div>
