<div class="leftbar">
    <!-- Start Sidebar -->
    <div class="sidebar">
        <!-- Start Navigationbar -->
        <div class="navigationbar">
            @include('layouts.left-menu.always-visible-bar')
            @include('layouts.left-menu.hideable-bar')
        </div>
        <!-- End Navigationbar -->
    </div>
    <!-- End Sidebar -->
</div>
