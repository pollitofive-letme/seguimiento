<div class="rightbar">
    @include('layouts.page-content.top-menu')
    <!-- End Topbar -->
    <div class="breadcrumbbar"  style="margin-bottom: 50px">
        @yield('content')
    </div>
    <!-- Start Footerbar -->
    @include('layouts.page-content.footer')
    <!-- End Footerbar -->
</div>
