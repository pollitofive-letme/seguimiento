<li class="list-inline-item">
    <div class="profilebar">
        <div class="dropdown">
            <a class="dropdown-toggle" href="#" role="button" id="profilelink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if((Auth::user()->genero === 'femenino'))
                    <img src="{{ asset('assets/images/users/girl.svg')}}" class="img-fluid" alt="profile">
                @else
                    <img src="{{ asset('assets/images/users/boy.svg')}}" class="img-fluid" alt="profile">
                @endif
                <span class="live-icon">{{ Auth::user()->nombre_completo }}</span>
                <span class="feather icon-chevron-down live-icon"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profilelink">
                <div class="dropdown-item">
                    <div class="profilename">
                        <h5>{{ Auth::user()->nombre_completo }}</h5>
                    </div>
                </div>
                <div class="userbox">
                    <ul class="list-unstyled mb-0">
                        <li class="media dropdown-item">
                            <a href="{{ route('profile') }}" class="profile-icon"><img src="{{ asset('assets/images/svg-icon/crm.svg') }}" class="img-fluid" alt="user">Mi perfil</a>
                        </li>
                        <li class="media dropdown-item">
                            <a href="{{ route('logout') }}" class="profile-icon" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><img src="{{ asset('assets/images/svg-icon/logout.svg') }}" class="img-fluid" alt="logout">Cerrar sesión</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</li>
