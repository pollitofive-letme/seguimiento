<div class="footerbar">
    <footer class="footer">
        <p class="mb-0">{{ date("Y")}} {{ env('APP_NAME') }} - Todos los derechos reservados.</p>
    </footer>
</div>
