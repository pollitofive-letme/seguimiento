{{-- @include('layouts.page-content.mobile') --}}
<!-- Start Topbar -->
<div class="topbar">
    <!-- Start row -->
    <div class="row align-items-center">
        <!-- Start col -->
        <div class="col-md-12 align-self-center">
            @include('layouts.page-content.open-close-menu')
            <div class="infobar">
                <ul class="list-inline mb-0">
                    @include('layouts.page-content.notifications')
                    @include('layouts.page-content.profile')
                </ul>
            </div>
        </div>
        <!-- End col -->
    </div>
    <!-- End row -->
</div>
