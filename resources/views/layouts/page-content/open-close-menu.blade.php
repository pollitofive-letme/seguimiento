<div class="togglebar" id="toggle-menu">
    <ul class="list-inline mb-0">
        <li class="list-inline-item">
            <div class="menubar">
                <a class="menu-hamburger" href="javascript:void();">
                    <img id="menu-opened" src="{{ asset('assets/images/svg-icon/menu.svg') }}" class="img-fluid menu-hamburger-collapse" alt="menu">
                    <img id="menu-closed" src="{{ asset('assets/images/svg-icon/close.svg') }}" class="img-fluid menu-hamburger-close" alt="close">
                </a>
            </div>
        </li>
        </li>
    </ul>
</div>
