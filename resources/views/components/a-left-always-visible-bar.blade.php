@props(['nombre' => '', 'imagen' => '', 'alt' => '','title' => ''])

<a class="nav-link active"
   id="v-{{$nombre}}-tab"
   data-toggle="{{ $nombre }}"
   href="#v-{{ $nombre }}s"
   role="tab"
   aria-controls="v-{{ $nombre }}s"
   aria-selected="true">
        <img src="{{ asset('assets/images/svg-icon/'.$imagen.'.svg') }}"
             class="img-fluid"
             alt="{{ $alt }}"
             data-toggle="tooltip"
             data-placement="top"
             title="{{ $title }}">
</a>
