@props(['route' => '', 'imagen' => ''])

<li><a href="{{url($route)}}"><img src="{{ asset('assets/images/svg-icon/'.$imagen.'.svg') }}" class="img-fluid" data-id="id-{{$slot}}-t" alt="{{ $slot }}">{{ $slot }}</a></li>
