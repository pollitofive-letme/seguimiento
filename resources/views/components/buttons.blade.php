<button type="submit" class="btn btn-rounded btn-outline-dark float-right" id="btnSave">
    <i class="sl-icon-arrow-right-circle"></i> Guardar
</button>
<a href="{{ URL::previous() }}" class="btn btn-rounded btn-outline-info float-right" id="btnBack" style="margin-right: 10px">
    <i class="sl-icon-arrow-left-circle"></i> Volver
</a>
