@props(['field' => '','value' => ''])
<div class="form-group">
    <label for="{{ $field }}">{{ ucfirst(str_replace("_"," ",$field)) }}</label>
    <input type="text" class="form-control" name="{{ $field }}"  value="{{ $value ?? ''}}">
</div>
