@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Bienvenido al {{ config('app.name_system') }}</div>

                @include('flash-message')
            </div>
        </div>
    </div>
@endsection
