@extends('layouts.app')

@section('script')
    <script src="{{ asset('assets/js/features/save.js') }}"></script>

    <script>

        document.getElementById("btnSave").addEventListener("click",function(event){
            event.preventDefault()
            const data = new FormData(document.getElementById('form'));
            save("{{ route('profile') }}",data);
        });

    </script>

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card m-b-30">
                <div class="card-header">
                    <h5 class="card-title">Mi perfil</h5>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('profile') }}" id="form">
                        @csrf
                        <div class="form-group">
                            <label for="legajo">Legajo</label>
                            <input type="number" class="form-control" name="legajo" placeholder="Ingresa tu legajo" value="{{ Auth::user()->legajo }}">
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="nombre" placeholder="Ingresa tu nombre" value="{{ Auth::user()->nombre }}">
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control" name="apellido" placeholder="Ingresa tu apellido" value="{{ Auth::user()->apellido }}">
                        </div>
                        <div class="form-group">
                            <label for="dni">DNI</label>
                            <input type="text" class="form-control" name="dni" placeholder="Ingresa tu dni" value="{{ Auth::user()->dni }}">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" name="email" placeholder="Ingresa tu email" value="{{ Auth::user()->email }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Género</label>
                            <div class="form-group">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="genero1" name="genero" class="custom-control-input" value="masculino" {{ ( Auth::user()->genero === 'masculino') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="genero1">Masculino</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="genero" id="genero2" class="custom-control-input" value="femenino" {{ ( Auth::user()->genero === 'femenino') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="genero2">Femenino</label>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-success float-right" id="btnSave">
                            <i class="feather icon-save mr-2"></i> Guardar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
