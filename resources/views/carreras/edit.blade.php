@extends('layouts.app')
@section('script')
    <script src="{{ asset('assets/js/features/save.js') }}"></script>

    <script>
        document.getElementById("btnSave").addEventListener("click",function(event){
            document.getElementById("btnSave").disabled = true;
            event.preventDefault()
            const data = new FormData(document.getElementById('form'));
            save("{{ route("carrera.update",$carrera->id) }}",data,"{{ url('carrera') }}");
        });

    </script>
@endsection
@section('content')

    @include('carreras.form',['action' => 'Editar','method' => 'put'])

@endsection
