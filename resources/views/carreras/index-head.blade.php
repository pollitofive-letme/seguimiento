@section('script')

    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/custom-table-datatable.js') }}"></script>
    <script src="{{ asset('assets/js/tooltip.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/features/swal-delete-from-list.js') }}"></script>
    <script src="{{ asset('assets/js/features/swal-activate-from-list.js') }}"></script>

    <script>

        var elements = document.getElementsByClassName("delete");

        var myFunction = function() {
            var id = this.getAttribute("data-id");
            var url = this.getAttribute("data-url");
            var descripcion = this.getAttribute("data-description");

            swalDeleteFromList(descripcion,url,'carrera');
        };

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', myFunction, false);
        }
    </script>
    <script>

        var elements = document.getElementsByClassName("activate");

        var myFunction = function() {
            var id = this.getAttribute("data-id");
            var url = this.getAttribute("data-url");
            var descripcion = this.getAttribute("data-description");

            swalActivateFromList(id,descripcion,'carrera/activate','carrera');
        };

        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', myFunction, false);
        }
    </script>
@endsection
@section('style')
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
