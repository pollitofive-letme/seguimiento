<div class="row justify-content-center">
    <div class="col-lg-6">
        <div class="card m-b-30">
            <div class="card-header text-center">
                <h1 class="card-title">{{ $action }} carrera</h1>
            </div>
            <div class="card-body">
                <form method="post" id="form">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    @csrf
                    <x-div-input field="nombre" value="{{ $carrera->nombre ?? '' }}"></x-div-input>
                    <x-div-input field="descripcion_corta" value="{{ $carrera->descripcion_corta ?? '' }}"></x-div-input>
                    <x-div-input field="duracion" value="{{ $carrera->duracion ?? '' }}"></x-div-input>
                    <x-div-input field="modalidad" value="{{ $carrera->modalidad ?? '' }}"></x-div-input>
                    <x-buttons></x-buttons>
                </form>
            </div>
        </div>
    </div>
</div>
