@extends('layouts.app')

@include('carreras.index-head')

@section('content')
    @csrf

    <div class="card-header" style="border-bottom: 0">
        <h2 class="text-center">Listado de carreras
            <a type="button" class="btn btn-rounded btn-dark float-right " href="{{ route('carrera.create') }}">Agregar</a>
        </h2>
    </div>
    <div class="table-responsive">
        <table id="default-datatable" class="display table table-striped table-bordered dataTable dtr-inline">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripción corta</th>
                <th>Duración</th>
                <th>Modalidad</th>
                <th>Creado</th>
                <th>Actualizado</th>
                <th>Acciones</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            @foreach($carreras as $carrera)
                <tr id="tr-{{ $carrera->id }}">
                    <td>{{$carrera->id}}</td>
                    <td>{{ $carrera->nombre }}</td>
                    <td>{{ $carrera->descripcion_corta }}</td>
                    <td>{{ $carrera->duracion }}</td>
                    <td>{{ $carrera->modalidad }}</td>
                    <td>{{ $carrera->created_at }}</td>
                    <td>{{ $carrera->updated_at }}</td>
                    @if($carrera->deleted_at)
                        <td><span class="badge badge-danger-inverse">Eliminado</span></td>
                        <td>
                            <a href="#" class="text-danger activate" data-id="{{$carrera->id}}" data-description="{{$carrera->nombre}}" data-url="{{ url('carrera',$carrera->id) }}"><i class="feather icon-repeat" data-toggle="tooltip" data-placement="top" title="Activar"></i></a>
                        </td>
                    @else
                        <td><span class="badge badge-primary-inverse">Activo</span></td>
                        <td>
                            <a href="{{ url("carrera/{$carrera->id}/edit") }}" class="text-primary mr-2"><i class="feather icon-edit-2" data-toggle="tooltip" data-placement="top" title="Editar"></i></a>
                            <a href="#" class="text-danger delete" data-id="{{$carrera->id}}" data-description="{{$carrera->nombre}}" data-url="{{ url('carrera',$carrera->id) }}"><i class="feather icon-trash-2" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>



@endsection
