<div class="row justify-content-center">
    <div class="col-lg-6">
        <div class="card m-b-30">
            <div class="card-header text-center">
                <h1 class="card-title">{{ $action }} universidad</h1>
            </div>
            <div class="card-body">
                <form method="post" id="form">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    @csrf
                    <div class="form-group">
                        <label for="legajo">Nombre</label>
                        <input type="text" class="form-control" name="nombre" value="{{ $universidad->nombre ?? '' }}" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="nombre">Descripción corta</label>
                        <input type="text" class="form-control" name="descripcion_corta"  value="{{ $universidad->descripcion_corta ?? ''}}">
                    </div>

                    <x-buttons></x-buttons>
                </form>
            </div>
        </div>
    </div>
</div>
