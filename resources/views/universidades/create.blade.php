@extends('layouts.app')


@section('script')
    <script src="{{ asset('assets/js/features/save.js') }}"></script>

    <script>

        document.getElementById("btnSave").addEventListener("click",function(event){
            document.getElementById("btnSave").disabled = true;
            event.preventDefault()
            const data = new FormData(document.getElementById('form'));
            save("{{ route('universidad.store') }}",data,"{{ url('universidad') }}");
        });

    </script>

@endsection

@section('content')

    @include('universidades.form',['action' => 'Crear','method' => 'post'])

@endsection
