@extends('layouts.app')

@include('universidades.index-head')

@section('content')
    @csrf
    <div class="card m-b-30" style="margin-bottom: 100px">
        <div class="card-header" style="border-bottom: 0">
            <h2 class="text-center">Listado de universidades
                <a type="button" class="btn btn-rounded btn-dark float-right " href="{{ route('universidad.create') }}">Agregar</a>
            </h2>
        </div>
        <div class="table-responsive">
            <table id="default-datatable" class="display table table-striped table-bordered dataTable dtr-inline">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Descripción corta</th>
                    <th>Creado</th>
                    <th>Actualizado</th>
                    <th>Acciones</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                @foreach($universidades as $universidad)
                    <tr id="tr-{{ $universidad->id }}">
                        <td>{{$universidad->id}}</td>
                        <td>{{ $universidad->nombre }}</td>
                        <td>{{ $universidad->descripcion_corta }}</td>
                        <td>{{ $universidad->created_at }}</td>
                        <td>{{ $universidad->updated_at }}</td>
                        @if($universidad->deleted_at)
                            <td><span class="badge badge-danger-inverse">Eliminado</span></td>
                            <td>
                                <a href="#" class="text-danger activate" data-id="{{$universidad->id}}" data-description="{{$universidad->nombre}}" data-url="{{ url('universidad',$universidad->id) }}"><i class="feather icon-repeat" data-toggle="tooltip" data-placement="top" title="Activar"></i></a>
                            </td>
                        @else
                            <td><span class="badge badge-primary-inverse">Activo</span></td>
                            <td>
                                <a href="{{ url("universidad/{$universidad->id}/edit") }}" class="text-primary mr-2"><i class="feather icon-edit-2" data-toggle="tooltip" data-placement="top" title="Editar"></i></a>
                                <a href="#" class="text-danger delete" data-id="{{$universidad->id}}" data-description="{{$universidad->nombre}}" data-url="{{ url('universidad',$universidad->id) }}"><i class="feather icon-trash-2" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></a>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Descripción corta</th>
                    <th>Creado</th>
                    <th>Actualizado</th>
                    <th>Acciones</th>
                    <th>Estado</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
