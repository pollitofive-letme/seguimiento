<div class="row justify-content-center">
    <div class="col-lg-6">
        <div class="card m-b-30">
            <div class="card-header text-center">
                <h1 class="card-title">{{ $action }} materia</h1>
            </div>
            <div class="card-body">
                <form method="post" id="form">
                    <input type="hidden" name="_method" value="{{ $method }}">
                    @csrf
                    <x-div-input field="nombre" value="{{ $materia->nombre ?? '' }}"></x-div-input>
                    <x-div-input field="descripcion_corta" value="{{ $materia->descripcion_corta ?? '' }}"></x-div-input>
                    <x-div-input field="horas_semanales" value="{{ $materia->horas_semanales ?? '' }}"></x-div-input>
                    <x-div-input field="horas_cuatrimestrales" value="{{ $materia->horas_cuatrimestrales ?? '' }}"></x-div-input>
                    <x-div-input field="cuatrimestre" value="{{ $materia->cuatrimestre ?? '' }}"></x-div-input>
                    <div class="form-group">
                        <label for="anio">Año</label>
                        <input type="text" class="form-control" name="anio"  value="{{ $materia->anio ?? '' }}">
                    </div>
                    <x-buttons></x-buttons>
                </form>
            </div>
        </div>
    </div>
</div>
