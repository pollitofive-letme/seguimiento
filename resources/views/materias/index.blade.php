@extends('layouts.app')

@include('materias.index-head')

@section('content')
    @csrf

    <div class="card-header" style="border-bottom: 0">
        <h2 class="text-center">Listado de Materias
            <a type="button" class="btn btn-rounded btn-dark float-right " href="{{ route('materia.create') }}">Agregar</a>
        </h2>
    </div>
    <div class="table-responsive">
        <table id="default-datatable" class="display table table-striped table-bordered dataTable dtr-inline">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripción corta</th>
                <th>Horas semanales</th>
                <th>Horas cuatrimestrales</th>
                <th>Cuatrimestre</th>
                <th>Año</th>
                <th>Creado</th>
                <th>Actualizado</th>
                <th>Acciones</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            @foreach($materias as $materia)
                <tr id="tr-{{ $materia->id }}">
                    <td>{{$materia->id}}</td>
                    <td>{{ $materia->nombre }}</td>
                    <td>{{ $materia->descripcion_corta }}</td>
                    <td>{{ $materia->horas_semanales }}</td>
                    <td>{{ $materia->horas_cuatrimestrales }}</td>
                    <td>{{ $materia->cuatrimestre }}</td>
                    <td>{{ $materia->anio }}</td>
                    <td>{{ $materia->created_at }}</td>
                    <td>{{ $materia->updated_at }}</td>
                    @if($materia->deleted_at)
                        <td><span class="badge badge-danger-inverse">Eliminado</span></td>
                        <td>
                            <a href="#" class="text-danger activate" data-id="{{$materia->id}}" data-description="{{$materia->nombre}}" data-url="{{ url('materia',$materia->id) }}"><i class="feather icon-repeat" data-toggle="tooltip" data-placement="top" title="Activar"></i></a>
                        </td>
                    @else
                        <td><span class="badge badge-primary-inverse">Activo</span></td>
                        <td>
                            <a href="{{ url("materia/{$materia->id}/edit") }}" class="text-primary mr-2"><i class="feather icon-edit-2" data-toggle="tooltip" data-placement="top" title="Editar"></i></a>
                            <a href="#" class="text-danger delete" data-id="{{$materia->id}}" data-description="{{$materia->nombre}}" data-url="{{ url('materia',$materia->id) }}"><i class="feather icon-trash-2" data-toggle="tooltip" data-placement="top" title="Eliminar"></i></a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>



@endsection
