@extends('layouts.app')
@section('script')
    <script src="{{ asset('assets/js/features/save.js') }}"></script>

    <script>
        document.getElementById("btnSave").addEventListener("click",function(event){
            document.getElementById("btnSave").disabled = true;
            event.preventDefault()
            const data = new FormData(document.getElementById('form'));
            save("{{ route("materia.update",$materia->id) }}",data,"{{ url('materia') }}");
        });

    </script>
@endsection
@section('content')

    @include('materias.form',['action' => 'Editar','method' => 'put'])

@endsection
