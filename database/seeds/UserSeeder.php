<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'legajo'   => 14461,
            'nombre'   => 'Damián',
            'apellido' => 'Ladiani',
            'email'    => 'damianladiani@gmail.com',
            'dni'      => 33794702,
            'password' => bcrypt('12345678'),
            'tipo'     => 1,
            'estado'   => 0
        ]);
    }
}
