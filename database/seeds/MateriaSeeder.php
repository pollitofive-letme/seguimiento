<?php

use App\Models\Materia;
use Illuminate\Database\Seeder;

class MateriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Materia::create([
            'nombre' => 'Programación I',
            'descripcion_corta' => 'PROG1',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Sistema de Procesamiento de Datos',
            'descripcion_corta' => 'SPD',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Matemática',
            'descripcion_corta' => 'MAT',
            'horas_semanales' => 9,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 144,
        ]);
        Materia::create([
            'nombre' => 'Ingles I',
            'descripcion_corta' => 'INGL1',
            'horas_semanales' => 3,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 48,
        ]);
        Materia::create([
            'nombre' => 'Laboratorio de Computación I',
            'descripcion_corta' => 'LAB1',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Programación II',
            'descripcion_corta' => 'PROG2',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Arquitectura y Sistemas Operativos',
            'descripcion_corta' => 'AYSO',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Estadistica',
            'descripcion_corta' => 'EST',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Ingles II',
            'descripcion_corta' => 'INGL2',
            'horas_semanales' => 3,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 48,
        ]);
        Materia::create([
            'nombre' => 'Laboratorio de Computación II',
            'descripcion_corta' => 'LAB2',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Metodología de la Investigación',
            'descripcion_corta' => 'METINV',
            'horas_semanales' => 3,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 48,
        ]);
        Materia::create([
            'nombre' => 'Programación III',
            'descripcion_corta' => 'PROG3',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Organización Contable de la Empresa',
            'descripcion_corta' => 'CONT',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Organización Empresarial',
            'descripcion_corta' => 'ORGEMP',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Elementos de Investigación Operativa',
            'descripcion_corta' => 'ELEM',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Laboratorio de Computación III',
            'descripcion_corta' => 'LAB3',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Metodología de Sistemas I',
            'descripcion_corta' => 'METSISI',
            'horas_semanales' => 12,
            'cuatrimestre' => 2,
            'anio' => 2,
            'horas_cuatrimestrales' => 192,
        ]);
        Materia::create([
            'nombre' => 'Diseño y Administración de Bases de Datos',
            'descripcion_corta' => 'DADB',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Legislación',
            'descripcion_corta' => 'LEG',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Laboratorio de Computación IV',
            'descripcion_corta' => 'LAB4',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 2,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Práctica Profesional',
            'descripcion_corta' => 'PPS',
            'horas_semanales' => 0,
            'cuatrimestre' => 2,
            'anio' => 2,
            'horas_cuatrimestrales' => 60,
        ]);

        Materia::create([
            'nombre' => 'Matemática II',
            'descripcion_corta' => 'MAT2',
            'horas_semanales' => 3,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 48,
        ]);
        Materia::create([
            'nombre' => 'Inglés Técnico Avanzado I',
            'descripcion_corta' => 'INGLT1',
            'horas_semanales' => 3,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 48,
        ]);
        Materia::create([
            'nombre' => 'Base de Datos II',
            'descripcion_corta' => 'DB2',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Programación Avanzada I',
            'descripcion_corta' => 'PROGAVI',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Laboratorio V',
            'descripcion_corta' => 'LAB5',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Metodología de Sistemas II',
            'descripcion_corta' => 'METSIS2',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Redes',
            'descripcion_corta' => 'REDES',
            'horas_semanales' => 6,
            'cuatrimestre' => 1,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Matemática III',
            'descripcion_corta' => 'MAT3',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Inglés Técnico Avanzado II',
            'descripcion_corta' => 'INGLT2',
            'horas_semanales' => 3,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 48,
        ]);
        Materia::create([
            'nombre' => 'Investigación Operativa II',
            'descripcion_corta' => 'INVOPE2',
            'horas_semanales' => 3,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 48,
        ]);
        Materia::create([
            'nombre' => 'Programación Avanzada II',
            'descripcion_corta' => 'PROGAV2',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Metodología de Sistemas III',
            'descripcion_corta' => 'METSIS3',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Administración y Dirección de Proyectos',
            'descripcion_corta' => 'ADPROY',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Seminario',
            'descripcion_corta' => 'SEM',
            'horas_semanales' => 6,
            'cuatrimestre' => 2,
            'anio' => 1,
            'horas_cuatrimestrales' => 96,
        ]);
        Materia::create([
            'nombre' => 'Práctica Profesional',
            'descripcion_corta' => 'PPS2',
            'horas_semanales' => 0,
            'cuatrimestre' => 0,
            'anio' => 1,
            'horas_cuatrimestrales' => 60,
        ]);

    }
}
