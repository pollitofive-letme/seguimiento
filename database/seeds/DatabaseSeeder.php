<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(UniversidadSeeder::class);
        $this->call(CarreraSeeder::class);
        $this->call(MateriaSeeder::class);
    }
}
