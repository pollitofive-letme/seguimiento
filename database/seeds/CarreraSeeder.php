<?php

use App\Models\Carrera;
use App\Models\CarrerasXUniversidad;
use App\Models\Universidad;
use Illuminate\Database\Seeder;

class CarreraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $universidad = Universidad::find(1);

        $carrera = Carrera::create([
            'nombre' => 'Técnico superior en programación',
            'descripcion_corta' => 'TSP'
        ]);

        CarrerasXUniversidad::create([
            'carrera_id' => $carrera->id,
            'universidad_id' => $universidad->id
        ]);

        Carrera::create([
            'nombre' => 'Técnico superior en sistemas informáticos',
            'descripcion_corta' => 'TSSI'
        ]);

        CarrerasXUniversidad::create([
            'carrera_id' => $carrera->id,
            'universidad_id' => $universidad->id
        ]);


    }
}
