<?php

use App\Models\Universidad;
use Illuminate\Database\Seeder;

class UniversidadSeeder extends Seeder
{
    public function run()
    {
        Universidad::create([
            'nombre' => 'Universidad Tecnológica Nacional - Facultad Regional General Pacheco',
            'descripcion_corta' => 'UTN-FRGP'
        ]);

        factory(Universidad::class,49)->create();
    }
}
