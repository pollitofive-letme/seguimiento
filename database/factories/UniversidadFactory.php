<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Universidad;
use Faker\Generator as Faker;

$factory->define(Universidad::class, function (Faker $faker) {
    return [
        'nombre' => $faker->streetName,
        'descripcion_corta' => $faker->colorName
    ];
});
