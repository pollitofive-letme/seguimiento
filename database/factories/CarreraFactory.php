<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Carrera;
use Faker\Generator as Faker;

$factory->define(Carrera::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'descripcion_corta' => $faker->name,
        'duracion' => $faker->randomElement([3,4,5]),
        'modalidad' => $faker->randomElement(['Presencial','Distancia']),
    ];
});
