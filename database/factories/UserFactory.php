<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'legajo'   => $faker->numberBetween(1,20000),
        'nombre'   => $faker->name,
        'apellido' => $faker->lastName,
        'email'    => $faker->email,
        'dni'      => $faker->numberBetween(8000000,48000000),
        'password' => bcrypt('123456'),
        'tipo'     => 2,
        'estado'   => 0
    ];
});
