<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Materia;
use Faker\Generator as Faker;

$factory->define(Materia::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name(),
        'descripcion_corta' => $faker->name(),
        'horas_semanales' => $faker->randomElement([3,6]),
        'cuatrimestre' => $faker->randomElement([1,2]),
        'anio' => $faker->randomElement([1,2,3,4,5]),
        'horas_cuatrimestrales' => $faker->randomElement([48,96]),
    ];
});
